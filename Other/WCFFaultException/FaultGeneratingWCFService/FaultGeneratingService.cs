﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace FaultGeneratingWCFService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class FaultGeneratingService : IFaultGeneratingService
    {
        public string FaultGeneratingMethod(string a, string b)
        {
            if (a != b)
                throw new FaultException<CustomFault>(new CustomFault() { CustomErrorInfo = "Hello World!", ErrorCode = ErrorCode.ERROR1 });

            return "OK!";
        }
    }
}
