﻿using System;
using System.ServiceModel;
//using System.ServiceModel;
using UIKit;
using WCFClientPCL.FaultService;
//using WCFClientPCL.FaultService;

namespace WCF_CUSTOM_FAULT
{
    public partial class DetailViewController : UIViewController
    {
        public object DetailItem { get; set; }

        public DetailViewController(IntPtr handle) : base(handle)
        {
        }

        public void SetDetailItem(object newDetailItem)
        {
            if (DetailItem != newDetailItem)
            {
                DetailItem = newDetailItem;

                // Update the view
                ConfigureView();
            }
        }

        void ConfigureView()
        {
            // Update the user interface for the detail item
            if (IsViewLoaded && DetailItem != null)
                detailDescriptionLabel.Text = DetailItem.ToString();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.
            txtResponse.Text = @"####1. PLEASE HAVE THE SERVER WHICH GENERATES FAULTS RUNNING (SimpleFaultGeneratingServer Project)####
###2. Set the ip address of the server  ####

When you Execute the following methods, a WCF soap call is made to the server. The server generates a fault message.

In the first case the Fault Message does not include the <FaultActor> Element(Part of the SOAP Standard). In this case, Mono handles it just fine!

In the Second case, the <FaultActor> element is Present. Mono, through the PCL, throws a ""Not Implemented Exception""
This same case is handled just fine when this same PCL is referenced in a console App.Also included as part of the solution.

";
            ConfigureView();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }


        async partial void BtnNoFaultActor_TouchUpInside(UIButton sender)
        {
            try
            {
                string s = await WCFClientPCL.ProxyWrapper.TestGenerateFaultTaskBased("http://" + txtIP.Text + ":6968/NoFaultActorEndPoint", "a", "b");
                txtResponse.Text = s;
            }
            catch (WCFClientPCL.CustomUnWrappedException ex2)
            {
                txtResponse.Text = "We Caught the Exception We Wanted..." + ex2.Message;
            }
            catch (Exception ex)
            {
                txtResponse.Text = ex.Message;

            }
        }

        async  partial void BtnFaultActor_TouchUpInside(UIButton sender)
        {
            try
            {
                string s = await WCFClientPCL.ProxyWrapper.TestGenerateFaultTaskBased("http://" + txtIP.Text + ":6968/FaultActorEndPoint", "a", "b");
                txtResponse.Text = s;
            }
            catch (WCFClientPCL.CustomUnWrappedException ex2)
            {
                txtResponse.Text = "We Caught the Exception We Wanted..." + ex2.Message;
            }
            catch (Exception ex)
            {
                txtResponse.Text = ex.Message;
            }
        }







        //async partial void BtnNoFaultActor_TouchUpInside(UIButton sender)
        //{

            //    try
            //    {
            //        string s = await WCFClientPCL.ProxyWrapper.TestGenerateFaultTaskBased("http://" + txtIP.Text + ":6968/NoFaultActorEndPoint", "a", "b");
            //        txtResponse.Text = s;
            //    }
            //    catch(WCFClientPCL.CustomUnWrappedException ex2)
            //    {
            //        txtResponse.Text = "We Caught the Exception We Wanted..." + ex2.Message;
            //    }
            //    catch (Exception ex)
            //    {
            //        txtResponse.Text = ex.Message;

            //    }
            //}

            //async partial void BtnFaultActor_TouchUpInside(UIButton sender)
            //{
            //    try
            //    {
            //        string s = await WCFClientPCL.ProxyWrapper.TestGenerateFaultTaskBased("http://" + txtIP.Text + ":6968/FaultActorEndPoint", "a", "b");
            //        txtResponse.Text = s;
            //    }
            //    catch (WCFClientPCL.CustomUnWrappedException ex2)
            //    {
            //        txtResponse.Text = "We Caught the Exception We Wanted..." + ex2.Message;
            //    }
            //    catch (Exception ex)
            //    {
            //        txtResponse.Text = ex.Message;
            //    }
            //}


        }
}


