using Android.Content.Res;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace TestXamarinForms
{
    public class AssetFileReader : IFileReader
    {
        #region Fields
        private string _FileName;
        private AssetManager _AssetManager;
        #endregion

        #region Constructor
        public AssetFileReader(string fileName, AssetManager assetManager)
        {
            _FileName = fileName;
            _AssetManager = assetManager;
        }
        #endregion

        #region Implementation
        public async Task<byte[]> ReadFileAsync()
        {   
            return await Task.Run(() => 
            {

                byte[] assemblyBytes;
                var stream = _AssetManager.Open("DynamicTypes.dll");
                using (var binaryReader = new BinaryReader(stream))
                {
                    var byteList = new List<byte>();

                    //We should be using this property to get the length of the file but this errors on Android
                    //var length = binaryReader.BaseStream.Length;

                    //Set up a while loop
                    while (true)
                    {
                        try
                        {
                            //Read a byte
                            var theByte = binaryReader.ReadByte();

                            //Add the byte to the list
                            byteList.Add(theByte);
                        }
                        catch (Exception ex)
                        {
                            //We have come to the end of the file.
                            //Note: it would be much better if we could use the length, or if there were an EOF property, but these are not supported so it appears we need to rely on handling an exception
                            break;
                        }
                    }

                    //Convert the list to an array
                    assemblyBytes = byteList.ToArray();
                }

                return assemblyBytes;
            });
        }
        #endregion
    }
}