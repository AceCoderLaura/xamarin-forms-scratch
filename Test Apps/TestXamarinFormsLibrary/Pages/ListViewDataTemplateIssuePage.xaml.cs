﻿using TestXamarinFormsLibrary.Models;
using Xamarin.Forms;

namespace TestXamarinFormsLibrary.Pages
{
    public partial class ListViewDataTemplateIssuePage : ContentPage
    {
        public FruitList Items { get; set; }

        public ListViewDataTemplateIssuePage()
        {
            InitializeComponent();

            Items = new FruitList
            {
                new Fruit{ Name= "Item 1" },
                new Fruit{ Name= "Item 2" },
                new Fruit{ Name= "Item 3" },
                new Fruit{ Name= "Item 4" },
                new Fruit{ Name= "Item 5" }
            };

            MyListView.ItemsSource = Items;
        }

    }
}
