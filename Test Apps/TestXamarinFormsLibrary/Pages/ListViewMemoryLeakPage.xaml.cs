﻿using System;
using TestXamarinForms;
using TestXamarinFormsLibrary.Models;
using Xamarin.Forms;

namespace TestXamarinFormsLibrary.Pages
{
    public partial class ListViewMemoryLeakPage : ContentPage
    {
        public FruitList Items { get; set; }

        public ListViewMemoryLeakPage()
        {
            InitializeComponent();

            GoButton.Clicked += GoButton_Clicked;
            ReturnButton.Clicked += ReturnButton_Clicked;

            Go();
        }

        private void ReturnButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new MainPage();
        }

        private void GoButton_Clicked(object sender, System.EventArgs e)
        {
            Go();
        }

        private void Go()
        {
            var previousList = MyListView.ItemsSource as FruitList;
            if (previousList != null)
            {
                previousList.Clear();
            }

            Items = new FruitList
            {
                new Fruit{ Name= "Item 1" },
                new Fruit{ Name= "Item 2" },
                new Fruit{ Name= "Item 3" },
                new Fruit{ Name= "Item 4" },
                new Fruit{ Name= "Item 5" }
            };

            MyListView.ItemsSource = Items;

            GC.WaitForPendingFinalizers();
            GC.Collect();
        }
    }
}
