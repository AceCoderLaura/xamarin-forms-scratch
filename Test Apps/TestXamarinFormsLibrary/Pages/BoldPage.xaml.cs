﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestXamarinFormsLibrary.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BoldPage : ContentPage
	{
		public BoldPage ()
		{
			InitializeComponent ();
		}
	}
}