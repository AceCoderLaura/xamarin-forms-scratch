﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DisplayActionSheetPage : ContentPage
    {
        private const string LogoutErrorMessage = "Log Out";

        public DisplayActionSheetPage()
        {
            InitializeComponent();

            CrashAppButton.Clicked += CrashAppButton_Clicked;
            PoorDesignButton.Clicked += PoorDesignButton_Clicked;
        }

        private async void CrashAppButton_Clicked(object sender, EventArgs e)
        {
            //This is a what we want. Just a "Log Out", and "Cancel" button. Nothing in the list. But this crashes the app (at least in UWP)
            var option = await DisplayActionSheet("Your session has expired. Would you like to log out?", "Cancel", LogoutErrorMessage, null);
        }

        private async void PoorDesignButton_Clicked(object sender, EventArgs e)
        {
            //This one works, but it's really poor design.
            //The popup should just have a "Log Out" and a "Cancel" button
            var option = await DisplayActionSheet("Your session has expired. Would you like to log out?", "Cancel", null, LogoutErrorMessage);
        }
    }
}