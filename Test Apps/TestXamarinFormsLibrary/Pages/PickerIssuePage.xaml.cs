﻿
using TestXamarinForms.Models;
using TestXamarinFormsLibrary.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestXamarinFormsLibrary.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PickerIssuePage : ContentPage
    {
        #region Fields
        private readonly AdaptObservableCollection<Fruit> _Items;
        #endregion

        #region Constructor
        public PickerIssuePage()
        {
            InitializeComponent();

            _Items = new AdaptObservableCollection<Fruit>();
            _Items.Add(new Fruit { Name = "Banana" });
            _Items.Add(new Fruit { Name = "Peach" });
            _Items.Add(new Fruit { Name = "Mango" });
            ThePicker.ItemsSource = _Items;
            ThePicker.SelectedIndex = 1;
            TheButton.Clicked += TheButton_Clicked;
        }
        #endregion

        #region Event Handlers
        private void TheButton_Clicked(object sender, System.EventArgs e)
        {
            _Items.RaiseCollectionChangedReset();
        }
        #endregion
    }
}