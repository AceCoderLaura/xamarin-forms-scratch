﻿using Pages;
using System;
using TestXamarinForms.AsyncListView;
using TestXamarinFormsLibrary.Pages;
using Xamarin.Forms;

namespace TestXamarinForms
{
    public partial class MainPage 
    {
        public MainPage()
        {
            InitializeComponent();

            TabsButton.Clicked += TabsButton_Clicked;
            CarouselPageButton.Clicked += CarouselPageButton_Clicked;
            ButtonGridButton.Clicked += ButtonGridButton_Clicked;
            InvisiblePanelsButton.Clicked += InvisiblePanelsButton_Clicked;
            BehaviourPageButton.Clicked += BehaviourPageButton_Clicked;
            AttachedPropertyPage.Clicked += AttachedPropertyPage_Clicked;
            ValidationButton.Clicked += ValidationButton_Clicked;
            BindablePropertyButton.Clicked += BindablePropertyButton_Clicked;
            AsyncListView.Clicked += AsyncListView_Clicked;
            DateDisplayButton.Clicked += DateDisplayButton_Clicked;
            BindingContextEqualityButton.Clicked += BindingContextEqualityButton_Clicked;
            DisplayActionSheetButton.Clicked += DisplayActionSheetButton_Clicked;
            AutoTestPageButton.Clicked += AutoTestPageButton_Clicked;
            AutoTestPageButton2.Clicked += AutoTestPageButton2_Clicked;
            ListViewDataTemplatesButton.Clicked += ListViewDataTemplatesButton_Clicked;
            GoogleCalendarButton.Clicked += GoogleCalendarButton_Clicked;
            PickerIssueButton.Clicked += PickerIssueButton_Clicked;
            BoldPageButton.Clicked += BoldPageButton_Clicked;
            ListViewMemoryLeakButton.Clicked += ListViewMemoryLeakButton_Clicked;
        }

        private void ListViewMemoryLeakButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new ListViewMemoryLeakPage();
        }

        private void BoldPageButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new BoldPage();
        }

        private void PickerIssueButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new PickerIssuePage();
        }

        private void GoogleCalendarButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new CalendarPage();
        }

        private void ListViewDataTemplatesButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new ListViewDataTemplateIssuePage();
        }

        private void AutoTestPageButton2_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new AutoTestPage2();
        }

        private void AutoTestPageButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new AutoTestPage();
        }

        private void DisplayActionSheetButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new DisplayActionSheetPage();
        }

        private void BindingContextEqualityButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new BindingContextEqualityPage();
        }

        private void DateDisplayButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new DateDisplayPage();
        }

        private void AsyncListView_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new AsyncListViewPage();
        }

        private void BindablePropertyButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new BindablePropertyPage();
        }

        private void ValidationButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new ValidationPage();
        }

        private void AttachedPropertyPage_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new AttachedPropertyPage();
        }

        private void BehaviourPageButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new BehaviourPage();
        }

        private void InvisiblePanelsButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new InvisibleTabbedPage();
        }

        private void ButtonGridButton_Clicked(object sender, EventArgs e)
        {
            var cp = new CarouselPage();
            cp.Children.Add(new ContentPage { Content = new ButtonGrid { Content = new Label { Text = "1" } }, Title = "1" });
            cp.Children.Add(new ContentPage { Content = new ButtonGrid { Content = new Label { Text = "2" } }, Title = "2" });

            Application.Current.MainPage = cp;
        }

        private void CarouselPageButton_Clicked(object sender, EventArgs e)
        {
            var cp = new CarouselPage();
            cp.Children.Add(new ContentPage { Content = new Label { Text = "1" }, Title = "1" });
            cp.Children.Add(new ContentPage { Content = new Label { Text = "2" }, Title = "2" });

            Application.Current.MainPage = cp;

            cp.IsEnabled = false;
        }

        private void TabsButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new BasicTabbedPage();
        }

        private void IsEnabledIssueButton_Clicked(object sender, EventArgs e)
        {
            Application.Current.MainPage = new IsEnabledCascading();
        }
    }
}
