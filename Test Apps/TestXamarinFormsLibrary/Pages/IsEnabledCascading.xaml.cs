﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestXamarinFormsLibrary.Pages
{
    public partial class IsEnabledCascading : ContentPage
    {
        public IsEnabledCascading()
        {
            InitializeComponent();
            DisablePageButton.Command = new DisablePageCommand();
        }
    }
}