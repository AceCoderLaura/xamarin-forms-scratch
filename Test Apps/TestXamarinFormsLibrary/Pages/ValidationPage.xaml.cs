﻿using TestXamarinForms.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestXamarinForms
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ValidationPage : ContentPage
	{
		public ValidationPage ()
		{
			InitializeComponent ();
            BindingContext = new NotifyDataErrorInfo();
		}
	}
}