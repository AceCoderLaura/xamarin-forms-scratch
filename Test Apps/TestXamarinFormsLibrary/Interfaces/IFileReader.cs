using System.Threading.Tasks;

namespace TestXamarinForms
{
    public interface IFileReader
    {
        Task<byte[]> ReadFileAsync();
    }
}