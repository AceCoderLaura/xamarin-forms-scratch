﻿using Xamarin.Forms;

namespace TestXamarinForms
{
    public class ExtendedEntry : Entry
    {
        public static readonly BindableProperty TestProperty =
          BindableProperty.Create
          (
          nameof(Test),
          typeof(int),
          typeof(ExtendedEntry),
          defaultValue: 0,
          defaultBindingMode: BindingMode.TwoWay,
          propertyChanging: TestChanging
          );

        public int Test
        {
            get
            {
                return (int)GetValue(TestProperty);
            }
            set
            {
                SetValue(TestProperty, value);

                var test = (int)GetValue(TestProperty);

                App.Current.MainPage.DisplayAlert("Result", $"Expected Result: 1\r\nActual Result: { test.ToString()}", "OK");
            }
        }

        private static void TestChanging(BindableObject bindable, object oldValue, object newValue)
        {
            var ctrl = (ExtendedEntry)bindable;
            ctrl.Test = (int)newValue;
        }
    }
}
