﻿using System.Collections.Specialized;
using System.Reflection;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace TestXamarinForms.AsyncListView
{
    public class AdaptListView : ListView
    {
        #region Fields
        private object _TempSelectedItem;
        private INotifyCollectionChanged _PreviousItemsSource;
        #endregion

        #region Protected Overrides

        protected override void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);

            if (propertyName == nameof(ItemsSource))
            {
                if (_PreviousItemsSource != null)
                {
                    _PreviousItemsSource.CollectionChanged -= (sender, e) => Refresh();
                }

                _PreviousItemsSource = ItemsSource as INotifyCollectionChanged;

                var itemsSource = ItemsSource as INotifyCollectionChanged;
                if (ItemsSource !=null)
                {
                    itemsSource.CollectionChanged += (sender, e) => Refresh();
                }

                Refresh();
            }
        }

        protected override void OnBindingContextChanged()
        {
            var binding = this.GetBinding(SelectedItemProperty);
            if (binding == null)
            {
                return;
            }

            var type = BindingContext?.GetType();
            var typeInfo = type?.GetTypeInfo();
            var bindingContextSelectedItemProperty = typeInfo?.GetDeclaredProperty(binding.Path);
            _TempSelectedItem = bindingContextSelectedItemProperty?.GetValue(BindingContext, null);

            base.OnBindingContextChanged();
        }

        #endregion

        #region Private Methods

        private void Refresh()
        {
            foreach (var item in ItemsSource)
            {
                if (item != null && item.Equals(_TempSelectedItem))
                {
                    SelectedItem = item;
                    _TempSelectedItem = null;
                    break;
                }
            }
        }
        #endregion
    }
}
