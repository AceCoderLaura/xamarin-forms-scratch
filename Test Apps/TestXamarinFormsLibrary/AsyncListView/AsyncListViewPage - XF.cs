﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TestXamarinForms.AsyncListView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AsyncListViewPage : ContentPage
    {
        private void DisplayAlert(string title, string message)
        {
            DisplayAlert(title, message, "OK");
        }

        private void SetWaitIndicatorVisibility(bool isVisible)
        {
            TheActivityIndicator.IsRunning = isVisible;
            TheActivityIndicator.IsVisible = isVisible;
        }

        private void AttachEventHandlers()
        {
            NewModelButton.Clicked += NewModelButton_Clicked;
            SetToTwoButton.Clicked += SetToTwoButton_Clicked;
            DeselectButton.Clicked += DeselectButton_Clicked;
        }

        private void DeselectButton_Clicked(object sender, System.EventArgs e)
        {
            CurrentAsyncListViewModel.ItemModel = null;
        }

        private void SetToTwoButton_Clicked(object sender, System.EventArgs e)
        {
            OnSetToTwoButtonClicked();
        }

        private void NewModelButton_Clicked(object sender, System.EventArgs e)
        {
            CreateNewModel();
        }
    }
}
