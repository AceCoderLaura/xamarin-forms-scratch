﻿namespace TestXamarinForms.AsyncListView
{
    public partial class AsyncListViewPage
    {
        ItemModelProvider items;
        ItemModel two;

        private AsyncListViewModel CurrentAsyncListViewModel => BindingContext as AsyncListViewModel;

        public AsyncListViewPage()
        {
            InitializeComponent();

            CreateNewModel();

            items = (ItemModelProvider)TheGrid.Resources["items"];
            items.ItemsLoaded += Items_ItemsLoaded;

            AttachEventHandlers();
        }

        private void OnSetToTwoButtonClicked()
        {
            if (two == null)
            {
                DisplayAlert("Wait for the items to load", "Wait for the items to load");
                return;
            }

            CurrentAsyncListViewModel.ItemModel = two;
        }

        private void CreateNewModel()
        {
            //Note: if you replace the line below with this, the behaviour works:
            //BindingContext = new AsyncListViewModel { ItemModel = two };

            BindingContext = new AsyncListViewModel { ItemModel = GetNewTwo() };
        }

        private static ItemModel GetNewTwo()
        {
            return new ItemModel { Name = 2, Description = "Second" };
        }

        private void Items_ItemsLoaded(object sender, System.EventArgs e)
        {
            SetWaitIndicatorVisibility(false);
            two = items[1];
        }
    }
}