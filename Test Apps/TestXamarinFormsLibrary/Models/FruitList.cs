﻿using System.Collections.ObjectModel;
using TestXamarinForms.Models;

namespace TestXamarinFormsLibrary.Models
{
    public class FruitList : ObservableCollection<Fruit>
    {
    }

    public class Fruit : IRecord
    {
        public string Name { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
