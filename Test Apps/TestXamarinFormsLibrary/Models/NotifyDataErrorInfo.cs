﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace TestXamarinForms.Models
{
    public class NotifyDataErrorInfo : INotifyDataErrorInfo, INotifyPropertyChanged
    {
        #region Fields
        private string _TestString;
        private readonly Dictionary<string, Collection<string>> _Errors = new Dictionary<string, Collection<string>>();
        #endregion

        #region Events
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region Public Properties
        public bool HasErrors
        {
            get
            {
                return !"Test".Equals(_TestString);
            }
        }

        public string TestString
        {
            get
            {
                return _TestString;
            }

            set
            {
                _TestString = value;
                RaisePropertyChanged();
            }
        }
        #endregion

        #region Private Methods
        private void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (propertyName == nameof(TestString))
            {
                Validate();
            }

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        private void Validate()
        {
            _Errors.Clear();

            if (HasErrors)
            {
                _Errors.Add(nameof(TestString), new Collection<string> { "TestString must be 'Test'" });
                ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(nameof(TestString)));
            }
        }
        #endregion

        #region Public Methods
        public IEnumerable GetErrors(string propertyName)
        {
            if (_Errors.ContainsKey(propertyName))
            {
                return _Errors[propertyName];
            }

            return new Collection<string>();
        }
        #endregion
    }
}
